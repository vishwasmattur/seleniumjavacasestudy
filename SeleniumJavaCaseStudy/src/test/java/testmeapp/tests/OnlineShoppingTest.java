package testmeapp.tests;

import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import testmeapp.utility.Drivers;
import testmeapp.utility.ExcelUtil;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.AfterTest;

public class OnlineShoppingTest {
	WebDriver driver;
	ExtentSparkReporter htmlReporter;
	ExtentReports extent;
	ExtentTest logger;

	@BeforeTest
	public void startReportbeforeTest() {
		driver = Drivers.getDriver("chrome");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss-ms");
		String filepath = System.getProperty("user.dir")+"/extent-reports/"+sdf.format(new Date())+".html";
		htmlReporter = new ExtentSparkReporter(filepath);
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test(priority=1)
	public void testRegistration() throws InterruptedException {
		logger = extent.createTest("testRegistration");
		driver.get("https://lkmdemoaut.accenture.com/TestMeApp/fetchcat.htm#");
		Assert.assertEquals(driver.getTitle(),"Home");
		driver.findElement(By.linkText("SignUp")).click();
		Assert.assertEquals(driver.getTitle(),"Sign Up");
		driver.findElement(By.id("userName")).sendKeys("Lalitha4");
		String errmsg = driver.findElement(By.xpath("//*[@id=\"err\"]")).getText();
		Assert.assertEquals(errmsg, "New");
		driver.findElement(By.id("firstName")).sendKeys("Lalitha");
		driver.findElement(By.id("lastName")).sendKeys("Lalitha");
		driver.findElement(By.id("password")).sendKeys("Password123");
		driver.findElement(By.name("confirmPassword")).sendKeys("Password123");
		driver.findElement(By.xpath("//*[@id=\"gender\"]")).click();
		driver.findElement(By.xpath("//*[@value=\"Female\"]")).click();
		driver.findElement(By.id("emailAddress")).sendKeys("Lalitha@gmail.com");
		driver.findElement(By.id("mobileNumber")).sendKeys("1234567890");
		driver.findElement(By.id("dob")).sendKeys("01/01/1990");
		driver.findElement(By.id("address")).sendKeys("Whitefield, Bengaluru");
		Select question = new Select(driver.findElement(By.id("securityQuestion")));
		question.selectByValue("411011");
		driver.findElement(By.id("answer")).sendKeys("red");
		Thread.sleep(10000);
		driver.findElement(By.name("Submit")).click();
		Thread.sleep(3000);
		Assert.assertEquals(driver.getTitle(),"Login");
	}
	
	@Test(priority=2)
	public void testLogin() throws InterruptedException {
		logger = extent.createTest("testLogin");
		driver.get("https://lkmdemoaut.accenture.com/TestMeApp/fetchcat.htm#");
		Assert.assertEquals(driver.getTitle(),"Home");
		driver.findElement(By.linkText("SignIn")).click();
		driver.findElement(By.id("userName")).sendKeys("Lalitha");
		driver.findElement(By.id("password")).sendKeys("Password123");
		Thread.sleep(10000);
		driver.findElement(By.name("Login")).click();
		Assert.assertEquals(driver.getTitle(),"Home");
	}
	
	@Test(priority=3)
	public void testCart() {
		logger = extent.createTest("testCart");
		Assert.assertEquals(driver.getTitle(),"Home");
		driver.findElement(By.xpath("//*[@id=\"myInput\"]")).sendKeys("wear");
		driver.findElement(By.xpath("//input[@value=\"FIND DETAILS\"]")).click();
		driver.findElement(By.xpath("/html/body/section/div/div/div[2]/div[3]/div/div/div[2]/center/a")).click();
		driver.findElement(By.xpath("//*[@id=\"header\"]/div[1]/div/div/div[2]/div/a[2]")).click();
		Assert.assertEquals(driver.getTitle(), "View Cart");
		driver.findElement(By.xpath("/html/body/header/div/b/a[1]")).click();
		
	}

	@Test(priority = 4, dataProvider = "xldata")
	public void testPayment(String Username,String Password,String Bankname,String Transactionpassword) throws InterruptedException {
		logger = extent.createTest("testPayment");
		Assert.assertEquals(driver.getTitle(),"Home");
		driver.findElement(By.xpath("//*[@id=\"myInput\"]")).sendKeys("wear");
		driver.findElement(By.xpath("//input[@value=\"FIND DETAILS\"]")).click();
		driver.findElement(By.xpath("/html/body/section/div/div/div[2]/div[3]/div/div/div[2]/center/a")).click();
		driver.findElement(By.xpath("//*[@id=\"header\"]/div[1]/div/div/div[2]/div/a[2]")).click();
		driver.findElement(By.xpath("//*[@id=\"cart\"]/tfoot/tr[2]/td[5]/a")).click();
		driver.findElement(By.xpath("/html/body/b/div/div/div[1]/div/div[2]/div[3]/div/form[2]/input")).click();
		driver.findElement(By.xpath("//*[@id=\"swit\"]//*[text()='"+Bankname+"']")).click();
		Thread.sleep(10000);
		driver.findElement(By.xpath("//*[@id=\"btn\"]")).click();
		driver.findElement(By.name("username")).sendKeys(Username);
		driver.findElement(By.name("password")).sendKeys(Password);
		Thread.sleep(10000);
		driver.findElement(By.xpath("//*[@id=\"horizontalTab\"]/div[2]/div/div/div/div/form/div/div[3]/input")).click();
		driver.findElement(By.name("transpwd")).sendKeys(Transactionpassword);
		Thread.sleep(10000);
		driver.findElement(By.xpath("//*[@id=\"horizontalTab\"]/div[2]/div/div/div/div/form/div/div[2]/input")).click();
		driver.findElement(By.xpath("/html/body/header/div/div/ul/b/a[1]")).click();
		Assert.assertEquals(driver.getTitle(), "Home");
	}
	
	@DataProvider(name="xldata")
	public Object[][] getDataFromExcel() {
		return ExcelUtil.readDataFromExcel("Sheet2");
	}

	@AfterMethod
	public void getResultAfterMethod(ITestResult result) throws IOException {
		if(result.getStatus()==ITestResult.SUCCESS) {
			logger.log(Status.PASS,"This is in "+result.getMethod().getMethodName());
		}
		else if(result.getStatus()==ITestResult.FAILURE) {
			logger.log(Status.FAIL,"This is in "+result.getMethod().getMethodName());
			TakesScreenshot screenshot = (TakesScreenshot) driver;
			File srcfile = screenshot.getScreenshotAs(OutputType.FILE);
			String destFile = System.getProperty("user.dir")+"/extent-reports/captures/"+result.getMethod().getMethodName()+".png";
			FileUtils.copyFile(srcfile, new File(destFile));
			logger.addScreenCaptureFromPath(destFile);
			logger.log(Status.FAIL,result.getThrowable().getMessage());
		}
		else if(result.getStatus()==ITestResult.SKIP) {
			logger.log(Status.SKIP,"This is in "+result.getMethod().getMethodName());
		}
	}

	@AfterTest
	public void endReportAfterTest() {
		driver.close();
		extent.flush();
	}

}