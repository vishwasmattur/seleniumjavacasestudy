package testmeapp.utility;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class ExcelUtil {

	static public Object[][] readDataFromExcel(String sheetName)
	{
		File file  = new File("src/test/resources/testdata/PaymentData.xlsx");
		Object[][] obj = null;
		try {
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook workBook = new XSSFWorkbook(fis);
			XSSFSheet testdata = workBook.getSheet(sheetName);
			obj = new Object[testdata.getLastRowNum()][];
			for(int i=1;i<=testdata.getLastRowNum();i++) {
				obj[i-1]=new Object[testdata.getRow(i).getPhysicalNumberOfCells()];
				for(int j=0; j<testdata.getRow(i).getPhysicalNumberOfCells();j++) {
					obj[i-1][j]=testdata.getRow(i).getCell(j).toString();
				}
			}
			workBook.close();
			fis.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return obj;
	}
	
}
