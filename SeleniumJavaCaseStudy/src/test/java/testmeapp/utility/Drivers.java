package testmeapp.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Drivers {
	
	static public WebDriver getDriver(String browserName) {
		if(browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver","src/test/resources/Drivers/chromedriver.exe");
			return new ChromeDriver();
		}
		else if(browserName.equals("edge")) {
			System.setProperty("webdriver.edge.driver","src/test/resources/Drivers/msedgedriver.exe");
			return new EdgeDriver();
		}
		else if(browserName.equals("ie")) {
			System.setProperty("webdriver.ie.driver","src/test/resources/Drivers/IEDriverServer.exe");
			return new InternetExplorerDriver();
		}
		else {
			return null;
		}
	}
}
